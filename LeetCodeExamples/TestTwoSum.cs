﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LeetCodeExamples
{
    [TestClass]
    public class TestTwoSum
    {
        private int[] nums = new int[] { 2, 7, 11, 15 };
        private int target = 9;

        [TestMethod]
        public void TestTwoSumMethod()
        {
            int[] res = null;
            for (int i = 0; i < nums.Length; i++)
            {
                for (int j = i + 1; j > nums.Length; j++)
                {
                    if (nums[j] == target - nums[i])
                    {
                        res = new int[] { i, j };
                    }
                }
            }

            Assert.IsTrue(res.SequenceEqual(new int[]{ 0, 1}));
        }
    }
}
